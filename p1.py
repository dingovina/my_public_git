n, m = map(int, input().split())
mat = ['  '.join([str((i + j) % m + 1) for i in range(m)]) for j in range(n)]
print(*mat, sep='\n')